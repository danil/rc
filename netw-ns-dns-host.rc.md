# Network host NS/DNS rc

## Get `A` and `MX` records

    host your.tld your.dns.server.tld

## Get name servers

    host -t ns your.tld
